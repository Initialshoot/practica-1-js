function Personas(nombre, sexo, edad) {
    let persona = Object.create(constructorPersona);
    persona.nombre = nombre;
    persona.edad = edad;
    persona.sexo = sexo;
    return persona;
}

let constructorPersona = {
    anios: function() {
        return `${this.nombre} tiene ${this.edad} años y es de sexo ${this.sexo}`;
    },
    bailar: function() {
        return `${this.nombre} puede bailar`;
    }
};

/*** - Gerentes - ***/

function Gerentes(nombre, dp, sexo, edad, per) {
    let gerente = Personas(nombre, sexo, edad);
    Object.setPrototypeOf(gerente, constructorGerente);
    gerente.dp = dp;
    gerente.per = per;
    return gerente;
}

let constructorGerente = {
    departamento: function() {
        return `${this.nombre}, ${this.sexo}, Gerente de ${this.dp}, tiene una edad de ${this.edad} años.`;
    },

    administrarDepartamento: function() {
        return `${this.nombre} esta encargado de ${this.per} personas.`;
    },

    realizarNomina: function() {
        return `${this.nombre} debe de asignar un sualdo quincenal de $7500 pesos a sus empleados`;
    }
}
Object.setPrototypeOf(constructorGerente, constructorPersona);


let ger1 = new Gerentes("Adrián Gutírrez", "Electrónica", "Masculino", 35, 5);
console.log(ger1);
console.log(ger1.departamento());
console.log(ger1.administrarDepartamento());
console.log(ger1.realizarNomina());

/*** - Empleados - ***/

function Empleados(nombre, dp, pos, sexo, edad) {
    let empleado = Personas(nombre, sexo, edad);
    Object.setPrototypeOf(empleado, constructorEmpleado);
    empleado.dp = dp;
    empleado.pos = pos;
    return empleado;
}

let constructorEmpleado = {
    departamento: function() {
        return `${this.nombre}, ${this.sexo}, Empleado de ${this.dp}, con puesto en ${this.pos}, tiene una edad de ${this.edad} años.`;
    },

    realizarInventario: function() {
        return `${this.nombre} esta encargado de realizar el inventario del área de ${this.pos} al termino de cada semana.`;
    },

    hacerVenta: function() {
        return `${this.nombre} debe de realizar por lo menos 15 ventas semales o más de 25 para recibir un bono.`;
    }
}
Object.setPrototypeOf(constructorEmpleado, constructorPersona);


let emp1 = new Empleados("Sergio Rodríguez", "Electrónica", "Videojuegos", "Masculino", 35);
console.log(emp1);
console.log(emp1.departamento());
console.log(emp1.realizarInventario());
console.log(emp1.hacerVenta());


/*** - Imprimir de la consola al HTML - ***/

document.getElementById("gerentes").innerHTML = `<section><br><h1>Gerentes</h1><br><br></section>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Gerente</th>
      <th scope="col">Departamento</th>
      <th scope="col">Nomina</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">${ger1.departamento()}</th>
      <td>${ger1.administrarDepartamento()}</td>
      <td>${ger1.realizarNomina()}</td>
    </tr>
  </tbody>
</table>
<br><br>

<section><br><h1>Empleados</h1><br><br></section>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Gerente</th>
      <th scope="col">Inventario</th>
      <th scope="col">Ventas</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">${emp1.departamento()}</th>
      <td>${emp1.realizarInventario()}</td>
      <td>${emp1.hacerVenta()}</td>
    </tr>
  </tbody>
</table>
`;